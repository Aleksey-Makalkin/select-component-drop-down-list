export enum RequestInfo {
    OriginLink = 'https:/alanbase.vercel.app/api/users',
    RequestLimit = 50
}

export type UserId = number

export type SelectedUserId = UserId | null

export type UserFirstName = string

export type UserLastName = string

export type UserEmail = string

export type UserJobTitle = string

export interface UserData {
    id: UserId;
    first_name: UserFirstName;
    last_name: UserLastName;
    email: UserEmail;
    job?: UserJobTitle;
}

export type ListOfUsers = UserData[]

export interface DataUsersList {
    meta: {
        from: number;
        to: number;
        total: number;
    };
    data: ListOfUsers
}