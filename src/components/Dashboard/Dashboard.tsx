import { FC, useState } from 'react'
import Select from '../Select/Select'
import SelectedUserInfo from '../SelectedUserInfo/SelectedUserInfo'
import './Dashboard.css'
import { RequestInfo, SelectedUserId } from '../../types/types'

const Dashboard: FC = () => {
    const [selectedUserId, setSelectedUserId] = useState<SelectedUserId>(null)
    const dataOriginUrl: string = RequestInfo.OriginLink
    
    return (
        <div className='Dashboard'>
            <Select dataOriginUrl={dataOriginUrl} selectedUserId={selectedUserId} setSelectedUserId={setSelectedUserId} />
            <SelectedUserInfo selectedUserId={selectedUserId} />
        </div>
    )
}

export default Dashboard