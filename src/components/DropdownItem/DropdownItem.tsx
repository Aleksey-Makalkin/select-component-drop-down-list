import { FC } from 'react';
import { SelectedUserId, UserFirstName, UserId, UserJobTitle, UserLastName } from '../../types/types'
import './DropdownItem.css'

interface DropdownItemProps {
    id: UserId;
    firstName: UserFirstName;
    lastName: UserLastName;
    job?: UserJobTitle;
    clickDropdownItem: (userId: UserId) => void;
    selectedUserId: SelectedUserId;
}

const DropdownItem: FC<DropdownItemProps> = ({
    id, firstName, lastName, job, clickDropdownItem, selectedUserId
}) => {
    let dropdownItemClass = 'DropdownItem'
    if (selectedUserId === id) {
        dropdownItemClass += ' DropdownItem_active'
    }

    return (
        <li className={dropdownItemClass} onClick={() => clickDropdownItem(id)}>
            <div className='dropdown_user_icon'>{lastName[0]}</div>
            <p className='dropdown_user_text'>{`${lastName} ${firstName}, ${job}`}</p>
        </li>
    )
}

export default DropdownItem