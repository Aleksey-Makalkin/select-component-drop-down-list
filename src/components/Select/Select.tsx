import { FC, ReactNode, useEffect, useRef, useState } from 'react'
import './Select.css'
import { DataUsersList, ListOfUsers, RequestInfo, SelectedUserId, UserId } from '../../types/types';
import DropdownItem from '../DropdownItem/DropdownItem';
import downIcon from '../../assets/icons/down_icon.svg'

type funcMy = (prev: SelectedUserId) => SelectedUserId

type SelectedUserType = SelectedUserId | funcMy

interface SelectProps {
    dataOriginUrl: string;
    setSelectedUserId: (prop: SelectedUserType) => void;
    selectedUserId: SelectedUserId;
}

const Select: FC<SelectProps> = ({dataOriginUrl, setSelectedUserId, selectedUserId}) => {
    const [isOpenList, setIsOpenList] = useState(false)
    const [listOfUsers, setListOfUsers] = useState<ListOfUsers>([])
    const [requestSheetNumber, setRequestSheetNumber] = useState<number>(1)
    const [isLoadingUsersList, setIsLoadingUsersList] = useState<boolean>(false)
    const [isAllDataReceived, setIsAllDataReceived] = useState<boolean>(false)
    const scrollContainerRef = useRef<HTMLElement>(null)
    const scrollContentRef = useRef<HTMLUListElement>(null)
    const resolveItemLimit: number = RequestInfo.RequestLimit

    const checkAllDataReceived = (data: DataUsersList): void => {
        if (data.meta.to === data.meta.total) {
            setIsAllDataReceived(true)
        }
    }

    useEffect(() => {
        if (isOpenList && !isLoadingUsersList && !isAllDataReceived) {
            if (!listOfUsers.length) {
                let pageNum: number = 1
                const fetchUrl = dataOriginUrl + `?page=${pageNum}&limit=${resolveItemLimit}`
                fetch(fetchUrl)
                .then(resolve => resolve.json())
                .then((data: DataUsersList) => {
                    checkAllDataReceived(data)
                    setListOfUsers(data.data)
                    setIsLoadingUsersList(false)
                })
                setIsLoadingUsersList(true)
            }
        }
    }, [isOpenList])
    useEffect(() => {
        if (isOpenList && !isLoadingUsersList && !isAllDataReceived) {
            const fetchUrl = dataOriginUrl + `?page=${requestSheetNumber}&limit=${resolveItemLimit}`
            fetch(fetchUrl)
            .then(resolve => resolve.json())
            .then((data: DataUsersList) => {
                checkAllDataReceived(data)
                setListOfUsers((prev: ListOfUsers): ListOfUsers => {
                    return ([
                        ... prev,
                        ... data.data
                    ])
                })
                setIsLoadingUsersList(false)
            })
            setIsLoadingUsersList(true)
        }
    }, [requestSheetNumber])

    const scrollList = throttle(() => {
        const scrollTop: number = Number(scrollContainerRef.current?.scrollTop)
        const containerHeight: number = Number(scrollContentRef.current?.offsetHeight)
        const visibleSectionHeight: number = Number(scrollContainerRef.current?.offsetHeight)
        const bottomMargin: number = containerHeight - visibleSectionHeight - 200
        if (scrollTop > bottomMargin
             && !isLoadingUsersList) {
            setRequestSheetNumber(prev => prev + 1)
        }
    }, 200)
    const clickDropdownButton = () => {
        setIsOpenList(prev => !prev)
    }
    const clickDropdownItem = (userId: UserId) => {
        setSelectedUserId(prev => prev === userId ? null : userId)
    }

    let dropdownListSectionClass = 'dropdown_list_section'
    if (!isOpenList) {
        dropdownListSectionClass += ' dropdown_list_section_hide'
    }
    let selectedUserTitle = 'Выберите пользователя...'
    const selectedUserIndex = listOfUsers.findIndex(user => user.id === selectedUserId)
    if (selectedUserIndex + 1) {
        const targetUser = listOfUsers[selectedUserIndex]
        selectedUserTitle = `${targetUser.last_name} ${targetUser.first_name}, ${targetUser.job}`
    }
    let loadedTitleElement: ReactNode = ''
    if (isLoadingUsersList) {
        loadedTitleElement = (
            <li className='additional_information_li'>
                <span>загрузка...</span>
            </li>
        )
    }
    if (isAllDataReceived) {
        loadedTitleElement = (
            <li className='additional_information_li'>
                <span>больше пользователей нет</span>
            </li>
        )
    }

    return (
        <div className='Select'>
            <section className='dropdown_value_section' onClick={clickDropdownButton}>
                <p className='dropdown_value_text'>{selectedUserTitle}</p>
                <img src={downIcon} />
            </section>
            <section className={dropdownListSectionClass} onScroll={scrollList} ref={scrollContainerRef}>
                <ul ref={scrollContentRef}>
                    {listOfUsers.map(userData => {
                        return <DropdownItem key={userData.id.toString()} id={userData.id} 
                        firstName={userData.first_name} lastName={userData.last_name} job={userData.job}
                        clickDropdownItem={clickDropdownItem} selectedUserId={selectedUserId} />
                    })}
                    {loadedTitleElement}
                </ul>
            </section>
        </div>
    )
}

function throttle(callee: (...args: any[]) => void, timeout: number) {
    let timer: number | undefined = undefined
    return function perform(...args: any[]): void {
        if (timer) return
        timer = setTimeout(() => {
            callee(...args)
            clearTimeout(timer)
            timer = undefined
        }, timeout)
    }
}

export default Select 