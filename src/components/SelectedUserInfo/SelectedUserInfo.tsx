import { FC } from 'react'
import { SelectedUserId } from '../../types/types'
import './SelectedUserInfo.css'

interface SelectedUserInfoProps {
    selectedUserId: SelectedUserId
}

const SelectedUserInfo: FC<SelectedUserInfoProps> = ({selectedUserId}: SelectedUserInfoProps) => {
    const idTitle: string = selectedUserId?.toString() || 'пользователь не выбран'

    return (
        <div className='SelectedUserInfo'>
            ID выбранного пользователя: {idTitle}
        </div>
    )
}

export default SelectedUserInfo